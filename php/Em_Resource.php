<?php

class Em_Resource {
	private static $staticContentPath = '/content/images/';
	private static $staticContentBaseUrl = 'http://tracking.emailsmaster.com';

	public static function isStaticContent($url) {
		return strpos(strtolower($url), self::$staticContentPath) !== false;
	}

	public static function rewrite($redirectUrl) {
		$filePath = parse_url($redirectUrl, PHP_URL_PATH);
		$localFilename = getenv('DOCUMENT_ROOT') . $filePath;

		if (file_exists($localFilename))
			return $redirectUrl;

		$remoteFilename = self::$staticContentBaseUrl . $filePath;
		$fileStream = fopen($remoteFilename, 'r', false, self::getStreamContext());

		if ($fileStream === false)
			return false;

		$localDirectory = dirname($localFilename);
		if (!file_exists($localDirectory))
			mkdir($localDirectory, 0775, true);

		$result = file_put_contents($localFilename, $fileStream);

		if ($result === false)
			return false;

		return $redirectUrl;
	}

	private static function getStreamContext() {
		$http_host = getenv('HTTP_HOST');

		$streamOptions = array(
			'http' => array(
				'method' => 'GET',
				'header' => "Origin: http://$http_host"
			)
		);

		return stream_context_create($streamOptions);
	}
}