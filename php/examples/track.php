<?php

require_once '../Em_Api.php';
require_once '../Em_Resource.php';

class Router {
	public static function dispatch($sourceUrl) {
		$redirectUrl = false;

		if (Em_Resource::isStaticContent($sourceUrl)) {
			$redirectUrl = Em_Resource::rewrite($sourceUrl);
		} else {
			$emApi = new Em_Api('http://api.em.xmonetize.net', 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'); // replace with your API key
			$redirectUrl = $emApi->getTrackingUrl($sourceUrl);
		}

		return $redirectUrl !== false
			? self::redirect($redirectUrl)
			: self::notFound();
	}

	public static function redirect($redirectUrl) {
		header("Location: $redirectUrl");
	}

	public static function notFound() {
		header('HTTP/1.1 404 Not Found');
	}
}

if (!isset($_GET['sourceurl'])) {
	Router::notFound();
	die();
}

Router::dispatch($_GET['sourceurl']);