<?php

$submitted = false;
$success = false;

$email = isset($_POST['email']) 
	? $_POST['email'] 
	: @$_GET['email'];

$tracker = isset($_POST['tracker'])
	? $_POST['tracker']
	: (isset($_GET['tracker'])
		? $_GET['tracker']
		: @$_GET['sourceUrl']);

$subscriberuid = isset($_POST['subscriberuid']) 
	? $_POST['subscriberuid'] 
	: (isset($_GET['subscriberuid'])
		? $_GET['subscriberuid']
		: '');

if (getenv('REQUEST_METHOD') == 'POST' || (isset($_GET['oneclick']) && $_GET['oneclick'])) {
	require_once '../Em_Api.php';

	$redirectUrl = false;

	$emApi = new Em_Api('http://api.em.xmonetize.net', 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXX');
	$success = $emApi->unsubscribe($tracker, $subscriberuid) !== false;

	$submitted = true;
}

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Subscription manager</title>
	<style>
		body { margin: 0; padding: 60px 0 0; background: #f9f8f8; font-family: sans-serif; font-size: 0.87em; color: #5a5a64; }
		h1 { margin: 0 0 40px; }
		form { padding: 40px; background: #fff; }
		label { font-weight: bolder; color: #97b811; }
		button { margin-top: 15px; padding: 7px 30px; color: #fff; background: #d9534f; border: 1px solid #d43f3a; border-radius: 3px; cursor: pointer; }
		button:hover { background-color: #c9302c; border-color: #ac2925; }

		.success { color: #97b811 }
		.error { color: #ff0000 }
	</style>
</head>
<body>
<form action="" method="post">
	<table width="690" align="center">
		<tr>
			<td align="left">
				<h1>Subscription manager</h1>
				<p>
					Please confirm your email address and click Unsubscribe below to be removed from our lists.
				</p>

				<?php if ($submitted): ?>
					<p>
						<?php if ($success): ?>
							<span class="success">You have successfully unsubscribed from our lists.</span>
						<?php else: ?>
							<span class="error">An error occurred. Please try again later.</span>
						<?php endif; ?>
					</p>
				<?php endif; ?>

				<table>
					<tr>
						<td height="45" width="123"><label for="email">Email Address:</label></td>
						<td><input type="email" name="email" id="email" value="<?=$email?>" required/></td>
					</tr>
				</table>

				<input name="tracker" type="hidden" value="<?=$tracker?>"/>
				<input name="subscriberuid" type="hidden" value="<?=$subscriberuid?>"/>

				<button>Unsubscribe</button>
			</td>
		</tr>
	</table>
</form>
</body>
</html>

