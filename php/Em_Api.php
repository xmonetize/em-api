<?php

class Em_Api {
	private $apiBaseUrl;
	private $timeout;

	function __construct($baseurl, $apikey, $timeout = 30) {
		$this->apiBaseUrl = rtrim($baseurl, '/') . '/api/' . $apikey . '/';
		$this->timeout = $timeout;
	}

	function getTrackingUrl($sourceUrl) {
		return $this->query('GET', 'Tracking', array(
			'sourceurl' => $sourceUrl,
			'mobile' => $this->isMobile() ? 'true' : 'false',
			'userip' => getenv('HTTP_X_REAL_IP') ?: getenv('REMOTE_ADDR'),
			'userAgent' => getenv('HTTP_USER_AGENT')
		));
	}

	function unsubscribe($tracker, $subscriberUid) {
		return false !== $this->query('DELETE', 'Unsubscribe', array(
			'tracker' => $tracker,
			'subscriberuid' => $subscriberUid
		));
	}

	private function isMobile() {
		$userAgent = getenv('HTTP_USER_AGENT');

		if (empty($userAgent))
			return false;

		if (strpos($userAgent, 'Mobile') !== false
			|| strpos($userAgent, 'Android') !== false
			|| strpos($userAgent, 'Silk/') !== false
			|| strpos($userAgent, 'Kindle') !== false
			|| strpos($userAgent, 'BlackBerry') !== false
			|| strpos($userAgent, 'Opera Mini') !== false
			|| strpos($userAgent, 'Opera Mobi') !== false ) {
			return true;
		}

		return false;
	}

	private function getApiMethodUrl($apiMethod, $queryString) {
		$url = $this->apiBaseUrl . $apiMethod;

		if ($queryString == null || empty($queryString))
			return $url;

		if (is_array($queryString))
			$queryString = http_build_query($queryString);

		return $url . '?' . $queryString;
	}

	private function query($method, $apiMethod, $queryString = null, $content = null) {
		$options = array(
			'http' => array(
				'method' => $method,
				'header' => "Content-Type: application/json\r\nAccept: application/json\r\n",
				'timeout' => $this->timeout,
				'ignore_errors' => true
			)
		);

		if ($content != null)
			$options['content'] = $content;

		$context = stream_context_create($options);

		$url = $this->getApiMethodUrl($apiMethod, $queryString);

		$response = @file_get_contents($url, false, $context);

		if ($response === false)
			return $response;

		return json_decode($response);
	}
}